# Practica 01 sobre Kubernetes

La siguiente practica es resultado del curso de Kubernetes de Platzi dictado por el profesor Marcos Nils.

Repositorio original: https://github.com/platzi/curso-kubernetes/

## Minikube Requeriments

 - `minikube start`
 - `minikube node add`
 - `minikube addons list`
 - `minikube addons enable metrics-server`

----

 - `kubectl apply -f k8s/redis-deploy.yml`
 - `kubectl apply -f k8s/hasher-deploy-svc.yml`
 - `kubectl apply -f k8s/rng-deploy-svc.yml`
 - `kubectl apply -f k8s/worker-deploy-svc.yml`
 - `kubectl apply -f k8s/webui-deploy-svc-elb-hpa.yml`
 
----

 - `minikube tunnel --bind-address=*`: Para luego acceder levantar el tunel con `ssh -L 8181:127.0.0.1:8181 remote_server` y poder acceder a la webui desde el navegador.

## Pruebas

Para probar el LoadBalancer (y apuntar a la IP "externa") que nos probee, es necesario ejecutar: **minikube tunnel**

Para probar el Horizontal Pod Autoscaler, se puede escalar el servicio webui:   
**kubectl scale deploy webui --replicas 2**

Sin trafico entrante se deberia reducir a 1 la cantidad de pods, de forma automatica.


